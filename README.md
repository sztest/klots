# Klots

Solve spatial logic puzzles in outer space.

## Playing the Game

Singleplayer or cooperative team play over LAN (or very low-latency internet) is recommended.  Running a general public multiplayer server is not currently recommended.

Gameplay instructions should not be necessary.  The game has very simple controls.  Explore and learn the mechanics as you go.

### Tips

- You can turn off music with `/set -n klots_music_volume 0` (or set it to any other ratio, default 1.0)
- If you believe you are softlocked and cannot reach a reset button, use the emergency `/kill` command.  This is considered a *bug*, so please report it!
- If you think you're an expert, try the `/speedrun` command and shoot for the best time.

### Engine Bug Warning

If you try this using Minetest engine versions from 5.6 to 5.7-dev before [#12906](https://github.com/minetest/minetest/pull/12906) was merged, make sure you have shaders *enabled*.  There is an engine bug in this version range (either [#12853](https://github.com/minetest/minetest/issues/12853) or [#12944](https://github.com/minetest/minetest/issues/12944)) that causes key components to be *invisible* in some levels.  This will be fixed in 5.7.

## Additional Resources

**Community**

- [Klots Discord](https://discord.gg/KMbCjAHBTs)

**Contributing**

- [Level Editing and Design Guide](https://gitlab.com/sztest/klots/-/blob/master/docs/level-editing.md)
- [Contribution Requirements](https://gitlab.com/sztest/klots/-/blob/master/CONTRIBUTING)

**Licensing**

- [Detailed Credits](https://gitlab.com/sztest/klots/-/blob/master/docs/credits.md)
- [Full License](https://gitlab.com/sztest/klots/-/blob/master/LICENSE)
