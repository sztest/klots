-- LUALOCALS < ---------------------------------------------------------
local klots, loadfile, minetest
    = klots, loadfile, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
loadfile(minetest.get_modpath(modname) .. "/exported.lua")(function(def)
		local myname = modname .. ":" .. def._raw_name:gsub("^nc_", ""):gsub(":", "_")
		def._raw_name = nil
		if def.pointable == nil then
			def.pointable = klots.editmode or def.walkable or def.walkable == nil or false
		end
		return minetest.register_item(myname, def)
	end)
