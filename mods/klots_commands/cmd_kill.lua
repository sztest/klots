-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, tonumber
    = klots, minetest, tonumber
-- LUALOCALS > ---------------------------------------------------------

minetest.unregister_chatcommand("kill")

minetest.register_chatcommand("kill", {
		description = "Reset the current level",
		privs = {interact = true},
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "invalid player" end

			local ppos = player:get_pos()
			local lv = klots.level_from_pos(ppos)
			if not lv then return false, "current level not found" end
			if not tonumber(lv) then return false, "invalid level" end

			return klots.level_reset(lv)
		end
	})
