-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

-- Hack to make the teleport command work even when floating in space
-- (ignore attachments, divert to player control system)

if klots.editmode then return end

local oldfunc = minetest.registered_chatcommands.teleport.func
minetest.registered_chatcommands.teleport.func = function(name, ...)
	local oldgetp = minetest.get_player_by_name
	function minetest.get_player_by_name(pname)
		local player = oldgetp(pname)
		if not player then return player end
		return {
			get_pos = function()
				return player:get_pos()
			end,
			set_pos = function(_, pos)
				minetest.get_player_by_name = oldgetp
				klots.player_teleport(player, player:get_pos(), pos)
			end,
			get_attach = function() end
		}
	end
	local function helper(...)
		minetest.get_player_by_name = oldgetp
		return ...
	end
	return helper(oldfunc(name, ...))
end
