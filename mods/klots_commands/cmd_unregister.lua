-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

local unreg = minetest.unregister_chatcommand

unreg("days")
unreg("rollback")
unreg("rollback_check")
unreg("time")

if klots.editmode then return end

unreg("clearinv")
unreg("give")
unreg("giveme")
unreg("pulverize")
