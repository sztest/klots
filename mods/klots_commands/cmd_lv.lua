-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, tonumber
    = klots, minetest, tonumber
-- LUALOCALS > ---------------------------------------------------------

-- Skip to a specific level, for editors or players w/ cheats

minetest.register_chatcommand("lv", {
		description = "Teleport to the start of the specified level",
		params = "<level or \"win\">",
		privs = (not klots.editmode) and {teleport = true} or nil,
		func = function(name, param)
			local lv = tonumber(param) or param == "win" and "win"
			local lvdata = lv and klots.leveldata[lv]
			if not lvdata then
				return false, "invalid level number"
			end
			local function helper(...)
				local player = minetest.get_player_by_name(name)
				if player then
					klots.player_camera_focus(player, player:get_pos())
				end
				return ...
			end
			return helper(minetest.registered_chatcommands.teleport.func(name,
					lvdata.pos.x .. " " .. lvdata.pos.y .. " " .. lvdata.pos.z))
		end
	})
