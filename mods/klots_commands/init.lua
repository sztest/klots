-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("cmd_unregister")
include("cmd_teleport")
include("cmd_lv")
include("cmd_kill")
include("cmd_exportlv")
include("cmd_speedrun")
