-- LUALOCALS < ---------------------------------------------------------
local klots, math, minetest, pairs, type
    = klots, math, minetest, pairs, type
local math_abs
    = math.abs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local skydata = {
	land = {
		override_day_night_ratio = 1,
		set_sky = {
			base_color = "#38588e",
			type = "skybox",
			textures = {
				modname .. "_land_1.jpg",
				modname .. "_land_2.jpg",
				modname .. "_land_3.jpg",
				modname .. "_land_4.jpg",
				modname .. "_land_5.jpg",
				modname .. "_land_6.jpg",
			},
			clouds = false
		},
		set_sun = {visible = true, texture = "[combine:1x1", sunrise_visible = false},
		set_moon = {visible = false},
		set_stars = {visible = false},
		set_lighting = {shadows = {intensity = 0.6}}
	},
	space = {
		override_day_night_ratio = 0.4,
		set_sky = {
			base_color = "#000000",
			type = "skybox",
			textures = {
				modname .. "_space_5.jpg^[transformR270",
				modname .. "_space_6.jpg^[transformR90",
				modname .. "_space_1.jpg",
				modname .. "_space_3.jpg",
				modname .. "_space_4.jpg",
				modname .. "_space_2.jpg",
			},
			clouds = false
		},
		set_sun = {visible = false, sunrise_visible = false},
		set_moon = {visible = false},
		set_stars = {visible = false},
		set_lighting = {shadows = {intensity = 0}}
	}
}

local function deepmatch(a, b)
	if type(a) ~= type(b) then return end
	if type(a) == "number" then
		if a == 0 then return b == 0 end
		if b == 0 then return a == 0 end
		local ratio = a / b
		return ratio > 0.9999 and ratio < 1.0001
	end
	if type(a) == "table" then
		for k, v in pairs(a) do
			if not deepmatch(v, b[k]) then return end
		end
		for k in pairs(b) do
			if a[k] == nil then return end
		end
		return true
	end
	return a == b
end

klots.register_playerstep(function(player, data)
		local setdata = skydata[player:get_pos().y > 128 and "space" or "land"]
		for k, v in pairs(setdata) do
			if not deepmatch(v, data[k]) then
				player[k](player, v)
				data[k] = v
			end
		end
	end)

local timeofday = 0.52
minetest.register_globalstep(function()
		if math_abs(minetest.get_timeofday() - timeofday) >= 0.004 then
			minetest.set_timeofday(timeofday)
		end
	end)
