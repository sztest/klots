-- LUALOCALS < ---------------------------------------------------------
local include, minetest
    = include, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local import_mod = {
	modname = modname,
	modpath = minetest.get_modpath(modname),
	storage = minetest.get_mod_storage()
}

include("modgen_decode.lua", import_mod)
include("modgen_util.lua", import_mod)
include("modgen_load_chunk.lua", import_mod)
include("modgen_register_mapgen.lua", import_mod)
include("modgen_read_manifest.lua", import_mod)
include("modgen_nodename_check.lua", import_mod)
include("modgen_localize_nodeids.lua", import_mod)
include("modgen_deserialize.lua", import_mod)

local manifest = import_mod.read_manifest()

if not manifest then
	minetest.log("warning", "MODGEN MANIFEST NOT FOUND")
	return
end

-- check if the nodes are available in the current world
minetest.register_on_mods_loaded(function()
		import_mod.nodename_check(manifest)
	end)

-- initialize mapgen
import_mod.register_mapgen(manifest)
