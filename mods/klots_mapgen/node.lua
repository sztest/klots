-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

klots.register_terrain({
		name = "dummy",
		drawtype = klots.editmode and "glasslike" or "airlike",
		tiles = klots.editmode and {modname .. "_visible.png"},
		paramtype = "light",
		sunlight_propagates = true,
		pointable = klots.editmode or false,
		description = "Invisible Terrain End"
	})

klots.register_terrain({
		name = "hull",
		drawtype = klots.editmode and "glasslike" or "airlike",
		tiles = klots.editmode and {modname .. "_visible.png"},
		paramtype = "light",
		sunlight_propagates = true,
		pointable = klots.editmode or false,
		description = "Invisible Collision Hull"
	})

klots.register_terrain({
		name = "focus",
		drawtype = klots.editmode and "plantlike" or "airlike",
		tiles = klots.editmode and {modname .. "_focus.png"},
		groups = {camera_focus = 1},
		paramtype = "light",
		sunlight_propagates = true,
		pointable = klots.editmode or false,
		walkable = false,
		buildable_to = not klots.editmode,
		description = "Camera Focus"
	})
