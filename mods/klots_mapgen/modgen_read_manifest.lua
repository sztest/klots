-- LUALOCALS < ---------------------------------------------------------
local io, minetest
    = io, minetest
local io_open
    = io.open
-- LUALOCALS > ---------------------------------------------------------

local import_mod = ...

local modname = minetest.get_current_modname()
local MP = minetest.get_modpath(modname)

function import_mod.read_manifest()
	local infile = io_open(MP .. "/manifest.json", "r")
	if not infile then return end

	local instr = infile:read("*a")
	infile:close()

	return minetest.parse_json(instr or "{}")
end
