-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, vector
    = klots, minetest, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

if klots.editmode then return end

local dummyname = modname .. ":dummy"

klots.register_playerstep(function(player)
		local pos = player:get_pos()
		if pos.y > 128 then return end
		local p = vector.round({
				x = pos.x,
				y = 8,
				z = pos.z
			})
		if minetest.get_node(p).name == dummyname then
			klots.player_teleport(player, pos, klots.leveldata[1].pos)
		end
	end)
