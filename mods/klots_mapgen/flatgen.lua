-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.set_mapgen_setting("mg_flags", "nocaves,nodungeons", true)
minetest.set_mapgen_setting("mgflat_spflags", "nolakes,nohills", true)

local dummyname = modname .. ":dummy"
minetest.register_alias("mapgen_stone", dummyname)
minetest.register_alias("mapgen_water_source", dummyname)
minetest.register_alias("mapgen_river_water_source", dummyname)
