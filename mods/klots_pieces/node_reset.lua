-- LUALOCALS < ---------------------------------------------------------
local error, klots, minetest
    = error, klots, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_node(modname .. ":reset_button", {
		description = "Reset Button",
		tiles = {"scifi_nodes_stripes.png^" .. modname .. "_reset_button.png"},
		paramtype = "light",
		light_souce = 5,
		on_neighbor_change = function(pos)
			klots.actuate_cooldown(pos, 1)
		end,
		on_actuate = function(pos)
			klots.actuate_cooldown(pos, 2)
			return klots.level_reset(pos)
		end
	})

local glass = minetest.registered_nodes["klots_terrain_nc:optics_glass"]
or error("nc glass not found")

minetest.register_node(modname .. ":reset_cover", {
		description = "Reset Button Molly-Guard",
		drawtype = "glasslike_framed",
		tiles = {glass.tiles[1]},
		sounds = glass.sounds,
		paramtype = "light",
		sunlight_propagates = true,
		on_actuate = function(pos)
			minetest.add_particlespawner({
					time = 0.1,
					amount = 100,
					collisiondetection = true,
					exptime = {min = 0.5, max = 4, bias = 1},
					texture = "^[combine:1x1^[noalpha^[colorize:#ffffff:255",
					size = {min = 0.25, max = 1, bias = 1
					},
					pos = {
						min = {x = pos.x - 0.5, y = pos.y - 0.5, z = pos.z - 0.5},
						max = {x = pos.x + 0.5, y = pos.y + 0.5, z = pos.z + 0.5}
					},
					vel = {
						min = {x = -0.5, y = -0.5, z = -0.5},
						max = {x = 0.5, y = 0.5, z = 0.5},
					},
				})
			klots.actuate_cooldown(pos, 1)
			klots.node_sound(pos, "dug", "dig")
			return minetest.set_node(pos, {name = modname .. ":reset_cover_broken"})
		end
	})

minetest.register_node(modname .. ":reset_cover_broken", {
		description = "Reset Button Molly-Guard",
		drawtype = "glasslike_framed",
		tiles = {{
				name = modname .. "_reset_broken.png",
				backface_culling = false
		}},
		groups = {not_in_creative_inventory = 1},
		pointable = false,
		sounds = glass.sounds,
		paramtype = "light",
		sunlight_propagates = true
	})
