-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("api_klotents")
include("api_klot_try_move")
include("node_klots")
include("node_teleport")
include("node_reset")
include("node_chime")
