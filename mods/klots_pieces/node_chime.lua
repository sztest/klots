-- LUALOCALS < ---------------------------------------------------------
local ipairs, klots, minetest
    = ipairs, klots, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local fanfare = {-3, 2, 5, 9, 14}

local function chime_play(pos)
	local lv = klots.level_from_pos(pos)
	for _, player in ipairs(minetest.get_connected_players()) do
		if klots.level_from_pos(player:get_pos()) == lv
		and not klots.music_suspend(player) then
			klots.music_suspend(player, 4)
			for i = 1, #fanfare do
				minetest.after(0.3 * i, function()
						klots.music_playnote(player, fanfare[i],
							0.5, 0.5, 0.5 + i * 0.05)
					end)
			end
		end
	end
end

klots.register_terrain({
		name = "chime",
		drawtype = klots.editmode and "plantlike" or "airlike",
		tiles = klots.editmode and {modname .. "_chime.png"},
		paramtype = "light",
		sunlight_propagates = true,
		pointable = klots.editmode or false,
		walkable = false,
		buildable_to = not klots.editmode,
		description = "Puzzle Chime",
		on_neighbor_change = (not klots.editmode) and minetest.remove_node or nil,
		after_destruct = (not klots.editmode) and chime_play or nil
	})
