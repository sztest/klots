-- LUALOCALS < ---------------------------------------------------------
local PcgRandom, klots, math, minetest, pairs, tonumber
    = PcgRandom, klots, math, minetest, pairs, tonumber
local math_floor, math_pow
    = math.floor, math.pow
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local key1 = {
	{2},
	{2},
	{0, 5, 2},
	{7, 5, 2},
}
local key2 = {
	{0},
	{0},
	{-3, 2, 0},
	{5, 2, 0}
}
local keys = {key1, key1, key2}

local bpm = 0.5
local function metronome() return math_floor(minetest.get_gametime() * bpm) end

local function playnote(t, data, tone, volume, echotime, echogain)
	if t < (data.no_notes or 0) then return end
	local id = minetest.sound_play(modname .. "_flute",
		{
			to_player = data.pname,
			pitch = math_pow(2, tone / 12),
			gain = 0.15 * volume
		})

	local buff = data.playing
	if not buff then
		buff = {}
		data.playing = buff
	end
	buff[id] = true
	minetest.after(5, function() buff[id] = nil end)

	if volume > 0.1 then
		minetest.after(echotime, function()
				return playnote(t + echotime * bpm, data,
					tone, volume * echogain,
					echotime, echogain)
			end)
	end
end

local getdata = klots.register_playerstep(function(player, data)
		local t = metronome()
		if t == data.last then return end
		data.last = t

		local overall_gain = tonumber(minetest.settings:get(modname .. "_volume")) or 1
		if overall_gain <= 0 then return end

		if player:get_pos().y < 128 then return end
		if t < (data.no_music or 0) then return end

		local keypos = math_floor(t / 16)
		local measure = math_floor(t / 4)
		local note = (t % 4) + 1

		local key = PcgRandom(keypos):next(1, #keys)
		local phrases = keys[key]
		local phrase = PcgRandom(measure):next(1, #phrases)
		local volume = PcgRandom(measure):next(1, 1000) *.0008 + 0.2
		local tone = phrases[phrase][note]
		if not tone then return end

		data.pname = data.pname or player:get_player_name()
		return playnote(t, data, tone, volume * overall_gain, 0.5, 0.6)
	end)

klots.register_on_player_teleport(function(player)
		local data = getdata(player)
		data.no_music = metronome() + 2
		data.no_notes = data.no_music
		if not data.playing then return end
		for id in pairs(data.playing) do
			minetest.sound_stop(id)
			data.playing[id] = nil
		end
	end)

function klots.music_playnote(player, tone, volume, echotime, echogain)
	local overall_gain = tonumber(minetest.settings:get(modname .. "_volume")) or 1
	if overall_gain <= 0 then return end

	local data = getdata(player)
	data.pname = data.pname or player:get_player_name()
	return playnote(metronome(), data, tone, volume * overall_gain, echotime, echogain)

end

function klots.music_suspend(player, qty)
	local data = getdata(player)
	local now = metronome()
	if qty and not (data.no_music and data.no_music > now + qty) then
		data.no_music = now + qty
		for id in pairs(data.playing) do
			minetest.sound_fade(id, -1, 0)
			data.playing[id] = nil
		end
	end
	return data.no_music and data.no_music > now
end
