-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_node(modname .. ":wieldhand", {
		description = "",
		drawtype = "mesh",
		mesh = modname .. "_hand.obj",
		tiles = {modname .. "_skin.png"},
		use_texture_alpha = "clip",
		wield_scale = {x = 2, y = 2, z = 2},
		groups = {not_in_creative_inventory = 1},
		stack_max = 1,
		node_placement_prediction = "",
		paramtype = "light",
		on_punch = minetest.remove_node
	})

minetest.register_on_joinplayer(function(player)
		local inv = player:get_inventory()
		inv:set_size("hand", 1)
		-- actual wieldhand stack managed by hud_dynamic
	end)
