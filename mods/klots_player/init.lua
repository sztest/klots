-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("player_basics")
include("player_hand")
include("player_inv")
include("player_visual")
include("player_wieldhand")
include("player_ouch")
include("player_hud_basics")
include("player_hud_dynamic")
include("player_hud_actuate")
include("player_newplayer")
