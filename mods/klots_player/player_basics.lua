-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

minetest.register_on_joinplayer(function(player)
		player:set_physics_override({speed = 1.6})
		player:set_properties({pointable = false})
		player:set_armor_groups({
				immortal = 1,
				fall_damage_add_percent = -100
			})
	end)
