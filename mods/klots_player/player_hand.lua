-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, pairs
    = klots, minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

if klots.editmode then
	minetest.register_tool(":", {
			wield_image = "wieldhand.png",
			on_use = function(_, user, pointed)
				klots.player_hand_movement(user)
				if pointed and pointed.under then
					minetest.remove_node(pointed.under)
				end
			end,
			on_place = function(_, user, pointed)
				klots.player_hand_movement(user)
				if not (user and pointed.under) then return end
				local item = minetest.get_node(pointed.under).name
				local inv = user:get_inventory()
				for i, s in pairs(inv:get_list("main")) do
					if s:get_name() == item then
						inv:set_stack("main", i, "")
					end
				end
				inv:set_stack("main", user:get_wield_index(), item)
			end
		})
else
	local interact = function(player, pointed)
		if not minetest.check_player_privs(player, "interact") then return end

		klots.player_hand_movement(player)

		if not (pointed and pointed.under) then return end
		local node = minetest.get_node(pointed.under)
		local def = minetest.registered_nodes[node.name]
		if not def then return end

		if def and def.on_actuate and not klots.actuate_cooldown(pointed.under) then
			if def.on_actuate(pointed.under, node, pointed) then return end
		end

		local pc = klots.player_control_get(player).param
		if (not pc) or (pc.moveaccel and pc.moveaccel > 0) then
			klots.node_sound(pointed.under, "footstep", "dig", "dug")
		end
	end
	minetest.register_tool(":", {
			wield_image = "wieldhand.png",
			on_use = function(_, player, pointed)
				return interact(player, pointed)
			end,
			on_place = function(_, player, pointed)
				return interact(player, pointed)
			end
		})
end
