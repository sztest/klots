-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

minetest.register_on_joinplayer(function(player)
		player:hud_set_hotbar_itemcount(klots.editmode and 10 or 1)
		player:hud_set_flags({
				hotbar = klots.editmode or false,
				healthbar = false,
				breathbar = false,
				minimap = false,
				minimap_radar = false,
				basic_debug = klots.editmode or false
			})
		if klots.editmode then
			player:hud_add({
					[klots.hudtype] = "text",
					position = {x = 0.5, y = 0.85},
					number = 0xff0000,
					text = "KLOTS MAP EDITING MODE",
					alignment = {x = 0, y = 0},
					offset = {x = 0, y = 0}
				})
		else
			klots.flash_screen(player)
		end
		player:set_properties({zoom_fov = 90})
		player:set_fov(90)
	end)
