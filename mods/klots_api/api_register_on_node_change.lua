-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, pairs
    = klots, minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

local changes = {}
klots.registered_on_node_changes = changes

function klots.register_on_node_change(func)
	changes[#changes + 1] = func
end

local hash = minetest.hash_node_position

local mask = {}

local function notify_node_change(pos, node, ...)
	local phash = hash(pos)
	if mask[phash] then return ... end
	mask[phash] = true
	node = node or minetest.get_node(pos)
	for i = 1, #changes do
		(changes[i])(pos, node, ...)
	end
	mask[phash] = nil
	return ...
end
klots.notify_node_change = notify_node_change

for fn, param in pairs({
		set_node = true,
		add_node = true,
		remove_node = false,
		swap_node = true,
		dig_node = false,
		place_node = true,
		add_node_level = false
	}) do
	local func = minetest[fn]
	klots[fn .. "_raw"] = func
	minetest[fn] = function(pos, pn, ...)
		return notify_node_change(pos, param and pn, func(pos, pn, ...))
	end
end
