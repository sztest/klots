-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, pairs
    = klots, minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

local function trysound(pos, sounds, event, ...)
	if not event then return end
	local sndspec = sounds[event]
	if not sndspec then return trysound(pos, sounds, ...) end
	local t = {}
	for k, v in pairs(sndspec) do t[k] = v end
	t.pos = pos
	minetest.sound_play(t.name, t, true)
	return true
end

function klots.node_sound(pos, ...)
	local node = minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name]
	if not def then return end
	local sounds = def.sounds
	if not sounds then return end
	return trysound(pos, sounds, ...)
end
