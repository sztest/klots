-- LUALOCALS < ---------------------------------------------------------
local io, klots, minetest
    = io, klots, minetest
local io_open
    = io.open
-- LUALOCALS > ---------------------------------------------------------

local worldpath = minetest.get_worldpath()
local handle = io_open(worldpath .. "/klots_edit")
if handle then
	klots.editmode = true
	handle:close()
end
