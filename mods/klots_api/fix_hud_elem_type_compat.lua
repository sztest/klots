-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

klots.hudtype = minetest.features.hud_def_type_field and "type" or "hud_elem_type"
