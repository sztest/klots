-- LUALOCALS < ---------------------------------------------------------
-- SKIP: include klots
local error, loadfile, minetest, rawget, rawset, table
    = error, loadfile, minetest, rawget, rawset, table
local table_concat
    = table.concat
-- LUALOCALS > ---------------------------------------------------------

local klots = {}
rawset(_G, "klots", klots)

local include = rawget(_G, "include") or function(path, ...)
	local parts = {
		minetest.get_modpath(minetest.get_current_modname()),
		path
	}
	if parts[#parts]:sub(-4) ~= ".lua" then
		parts[#parts] = parts[#parts] .. ".lua"
	end
	local func, err = loadfile(table_concat(parts, "/"))
	if err then return error(err) end
	return func(...)
end
rawset(_G, "include", include)

----------------------------------------

include("fix_hud_elem_type_compat")
include("fix_set_properties")
include("fix_auth_cache")

----------------------------------------

include("api_editmode")
include("api_leveldata")

include("api_scan_flood")
include("api_node_sound")
include("api_mapblock_dirty")
include("api_actuate_cooldown")

include("api_register_playerstep")
include("api_flash_screen")
include("api_player_control")
include("api_player_teleport")
include("api_level_reset")

include("api_register_item_modifier")
include("api_register_on_node_change")
include("api_register_terrain")

----------------------------------------

include("std_no_items")
include("std_item_creative")
include("std_param2fix")

include("std_auto_timer")
include("std_ambiance")
include("std_on_neighbor_change")
