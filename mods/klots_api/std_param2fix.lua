-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

if not klots.editmode then return end

klots.register_item_modifier(function(_, def)
		if def.place_param2 and def.place_param2 ~= 0 then
			def.groups = def.groups or {}
			def.groups.param2fix = 1
		end
	end)

minetest.register_abm({
		nodenames = {"group:param2fix"},
		inverval = 2,
		chance = 1,
		action = function(pos, node)
			if node.param2 ~= 0 then return end
			local def = minetest.registered_nodes[node.name]
			if def and def.place_param2 then
				node.param2 = def.place_param2
				return minetest.swap_node(pos, node)
			end
		end
	})
