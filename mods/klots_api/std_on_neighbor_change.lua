-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, pairs, vector
    = klots, minetest, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local node_faces = {
	{x = 0, y = -1, z = 0},
	{x = 0, y = 1, z = 0},
	{x = -1, y = 0, z = 0},
	{x = 1, y = 0, z = 0},
	{x = 0, y = 0, z = -1},
	{x = 0, y = 0, z = 1},
}

local neighbordefs = {}
minetest.register_on_mods_loaded(function()
		for k, v in pairs(minetest.registered_nodes) do
			if v.on_neighbor_change then
				neighbordefs[k] = v.on_neighbor_change
			end
		end
	end)
local vector_add = vector.add
klots.register_on_node_change(function(pos, ...)
		for i = 1, #node_faces do
			local p = vector_add(pos, node_faces[i])
			local node = minetest.get_node(p)
			local hook = neighbordefs[node.name]
			if hook then hook(p, node, pos, ...) end
		end
	end)
