-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, vector
    = klots, minetest, vector
-- LUALOCALS > ---------------------------------------------------------

local cooldowns = {}

local now = 0
minetest.register_globalstep(function(dtime) now = now + dtime end)

local round = vector.round
local hash = minetest.hash_node_position

function klots.actuate_cooldown(pos, ttl)
	local key = hash(round(pos))
	local cd = cooldowns[key] or 0
	if ttl then
		ttl = ttl + now
		if ttl > cd then
			cd = ttl
			cooldowns[key] = ttl
		end
	end
	return cd > now
end
