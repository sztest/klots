-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, vector
    = klots, minetest, vector
-- LUALOCALS > ---------------------------------------------------------

local dirs = {
	{x = 1, y = 0, z = 0},
	{x = -1, y = 0, z = 0},
	{x = 0, y = 1, z = 0},
	{x = 0, y = -1, z = 0},
	{x = 0, y = 0, z = 1},
	{x = 0, y = 0, z = -1},
}
function klots.scan_flood(pos, func)
	local hash = minetest.hash_node_position
	local q = {vector.round(pos)}
	local seen = {}
	local limit = 1024
	while true do
		local nxt = {}
		for i = 1, #q do
			local p = q[i]
			local res = func(p)
			if res then return res end
			if res == nil then
				for j = 1, #dirs do
					local v = dirs[j]
					local np = {
						x = p.x + v.x,
						y = p.y + v.y,
						z = p.z + v.z,
						prev = p,
						dir = v
					}
					local nk = hash(np)
					if not seen[nk] then
						seen[nk] = true
						nxt[#nxt + 1] = np
					end
				end
			end
		end
		if #nxt < 1 then break end
		limit = limit - #q
		if limit <= 0 then break end
		q = nxt
	end
end
