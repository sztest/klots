-- LUALOCALS < ---------------------------------------------------------
local klots, minetest, pairs, print, vector
    = klots, minetest, pairs, print, vector
-- LUALOCALS > ---------------------------------------------------------

local basedef = {
	stack_max = 1,
	on_use = function(_, _, pointed)
		if pointed and pointed.under then
			minetest.remove_node(pointed.under)
		end
	end,
	on_secondary_use = function(stack, user)
		if not user then return end
		local pos = user:get_pos()
		pos.y = pos.y + user:get_properties().eye_height
		pos = vector.add(pos, vector.multiply(user:get_look_dir(), 4))
		minetest.item_place(stack, user, {
				type = "node",
				above = pos,
				under = pos
			})
		klots.node_sound(pos, "place")
		-- return nil -> no consumption
	end,
	on_place = function(stack, user, pointed)
		minetest.item_place(stack, user, pointed)
		-- return nil -> no consumption
	end,
	on_drop = function() return "" end
}

klots.register_item_modifier(function(_, def)
		if def.type ~= "node" then return end
		for k, v in pairs(basedef) do
			if def[k] == nil then
				if _ == "klots_api:focus" then print(k) end
				def[k] = v
			end
		end
	end)
