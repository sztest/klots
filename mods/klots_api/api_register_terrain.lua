-- LUALOCALS < ---------------------------------------------------------
local klots, minetest
    = klots, minetest
-- LUALOCALS > ---------------------------------------------------------

function klots.register_terrain(def)
	local name = def.name
	if def.pointable == nil then
		def.pointable = klots.editmode or def.walkable or def.walkable == nil or false
	end
	minetest.register_node(minetest.get_current_modname() .. ":" .. name, def)
end
