-- LUALOCALS < ---------------------------------------------------------
local ipairs, klots, minetest, next, pairs, rawget, rawset,
      setmetatable
    = ipairs, klots, minetest, next, pairs, rawget, rawset,
      setmetatable
-- LUALOCALS > ---------------------------------------------------------

local regs = {}
klots.registered_item_modifiers = regs

function klots.register_item_modifier(func)
	regs[#regs + 1] = func
	for name, itemdef in pairs(minetest.registered_items) do
		local overrides = {}
		local t = {}
		setmetatable(t, {
				__index = function(_, k)
					return rawget(itemdef, k)
				end,
				__newindex = function(_, k, v)
					overrides[k] = v
					return rawset(itemdef, k, v)
				end
			})
		func(name, t)
		if next(overrides) then
			minetest.override_item(name, overrides)
		end
	end
end

local oldreg = minetest.register_item
function minetest.register_item(name, def, ...)
	for _, v in ipairs(klots.registered_item_modifiers) do
		local x = v(name, def, ...)
		if x then return x end
	end
	return oldreg(name, def, ...)
end
