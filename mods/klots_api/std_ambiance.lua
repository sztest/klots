-- LUALOCALS < ---------------------------------------------------------
local klots, math, minetest, pairs
    = klots, math, minetest, pairs
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local ambiance = {}
minetest.register_on_mods_loaded(function()
		for k, v in pairs(minetest.registered_nodes) do
			if v.groups.ambiance then
				ambiance[k] = v.groups.ambiance / 100
			end
		end
	end)

local hash = minetest.hash_node_position
local offset = {}
minetest.register_abm({
		nodenames = {"group:ambiance"},
		interval = 1,
		chance = 1,
		action = function(pos, node)
			if math_random() >= ambiance[node.name] then return end
			local key = hash(pos)
			local offs = offset[key]
			if not offs then
				offs = math_random()
				offset[key] = offs
			end
			minetest.after(offs, function()
					klots.node_sound(pos, "ambiance")
				end)
		end
	})
