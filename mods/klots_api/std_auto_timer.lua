-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local squelch = {}

local hash = minetest.hash_node_position
local function autostart(pos)
	local key = hash(pos)
	if squelch[key] then return end
	squelch[key] = true
	return minetest.get_node_timer(pos):start(0.001)
end

minetest.register_lbm({
		name = modname .. ":auto_timer",
		run_at_every_load = true,
		nodenames = {"group:auto_timer"},
		action = autostart
	})

minetest.register_abm({
		nodenames = {"group:auto_timer"},
		interval = 2,
		chance = 1,
		action = autostart
	})
