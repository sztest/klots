-- LUALOCALS < ---------------------------------------------------------
local klots, loadfile, minetest, pairs, type
    = klots, loadfile, minetest, pairs, type
-- LUALOCALS > ---------------------------------------------------------

local overrides = {
	junk = {
		walkable = true,
		sounds = minetest.registered_nodes["klots_terrain_nc:terrain_gravel"].sounds
	}
}

local function texturefix(obj)
	if type(obj) == "string" then
		return obj:gsub("default_dirt.png", "nc_terrain_dirt.png")
		:gsub("default_grass.png", "nc_terrain_grass_top.png")
		:gsub("default_grass.png", "(nc_terrain_grass_top.png^[mask:nc_terrain_grass_sidemas.png)")
	end
	if type(obj) == "table" then
		for k, v in pairs(obj) do
			obj[k] = texturefix(v)
		end
	end
	return obj
end

local soundmap = {
	default_dig_metal = "nc_lode_annealed",
	default_wood_footstep = "nc_tree_woody",
	default_metal_footstep = "nc_lode_annealed",
	default_hard_footstep = "nc_optics_glassy",
	default_dug_metal = "nc_lode_annealed",
	default_place_node_hard = "nc_terrain_stony",
	default_place_node_metal = "nc_lode_annealed",
	default_dig_choppy = "nc_tree_woody",
	default_break_glass = "nc_optics_glassy",
	default_glass_footstep = "nc_optics_glassy"
}

local function mergetable(a, b)
	if type(b) == "table" then
		a = a or {}
		for k, v in pairs(b) do
			a[k] = mergetable(a[k], v)
		end
		return a
	end
	return b
end

local modname = minetest.get_current_modname()
loadfile(minetest.get_modpath(modname) .. "/exported.lua")(function(def)
		local suff = def._raw_name:gsub(".*:", "")
		local myname = modname .. ":" .. suff
		local custom = overrides[suff]
		if custom then def = mergetable(def, custom) end
		def._raw_name = nil
		if def.pointable == nil then
			def.pointable = klots.editmode or def.walkable or def.walkable == nil or false
		end
		texturefix(def.tiles)
		texturefix(def.special_tiles)
		texturefix(def.inventory_image)
		texturefix(def.wield_image)
		for _, v in pairs(def.sounds or {}) do
			if type(v) == "table" and v.name then
				v.name = soundmap[v.name] or v.name
			end
		end
		return minetest.register_item(myname, def)
	end)
