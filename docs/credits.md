# Klots Credits

- Everything not listed below is by:
	- Aaron Suen (MIT)  


- klots_music
	- klots_music_flute.ogg
		- Wood_Flutes (CC-BY 3.0)
		- https://freesound.org/people/Wood_Flutes/sounds/264942/
- klots_player
	- klots_player_model.b3d
		- LoneWolfHT and MisterE (MIT)
		- From NodeCore
	- klots_player_hand.obj
		- LoneWolfHT (MIT)
		- From NodeCore
	- klots_player_skin.png
		- WintersKnight94 (MIT)
		- From NodeCore, heavily modified
- klots_skybox
	- space skybox textures
		- Westbeam (CC0 / WTFPL)
		- https://opengameart.org/content/space-skyboxes-1
	- land skybox textures
		- Emil Persson (CC-BY 3.0)
		- https://opengameart.org/content/mountain-skyboxes
- klots_terrain_scifi
	- all textures, and nodebox designs
		- DOOMED (CC-BY-SA 3.0 UNPORTED)
		- From scifi_nodes mod
- klots_terrain_nc
	- nc_flora_* textures
		- Warr1024 (MIT)
		- WintersKnight94 (MIT)
		- From NodeCore
- klots_mapgen
	- modgen_*.lua
		- BuckarooBanzay (MIT)
		- From the modgen export mod
