# Klots Editing and Level Design

Level editing is somewhat more complex than just enabling "creative mode" (and thus the option has been disabled to prevent accidental activation).  To edit the map, you need to create a special separate "editing" world:

- Create a new klots world.
- Install the [modgen](https://content.minetest.net/packages/BuckarooBanzay/modgen/) mod.
	- This is required to be able to install your map changes into the game itself, including for play-testing them.
- Add a file named `klots_edit` in the root of the world dir.
- Enter the editing world and grant yourself the necessary permissions (teleport,fly,fast,noclip etc).
- Use the `/exportall` command at least once to setup initial modgen export of all existing levels.

## Editing Mode Controls

- You can use the `/lv` chat command to teleport to a level.  The place you end up at is exactly where the player will start in the level.
- Browse and search the set of available nodes in the edit mode inventory screen.
- Right-click a node with an empty hand to pick up a copy of the node.  If it already exists somewhere in your inventory, it will be *moved* to the active slot.
- To free up inventory space, you can simply drop items; they are destroyed.  Use `/clearinv` to do the whole thing.
- Tapping the zoom key (ignore the message) swaps the 2 rows in your inventory, swapping out which items are in your hotbar, for easy access to your whole inventory.
- If you right-click to place a node while not pointing at another node, it will place that node at the extent of your reach (4 nodes away) in mid-air; use this to start mid-air structures.
- The `/exportlv` command will export the level you are currently in.
- You can use `/exportall` to export all levels at any time.

## Level Management

- The total number of levels is stored in a constant in `mods/klots_api/api_leveldata.lua`; increase it to add more levels.
- By default, each end-of-level teleporter takes the player to the next level, or to the end credits on the last level.  Multiple exits in a single level will take the player to the same place (alternate destinations are not _yet_ supported).
- Overrides can be added at the bottom of `mods/klots_api/api_leveldata.lua`, but should be used _sparingly_.

## Special Nodes

- The "Invisible Terrain End" node is only to be used in "terrestrial" areas (i.e. level 1).
	- It's what the "ground" is made out of via flat mapgen.
	- It teleports the player to spawn if stepped over.
- The "Invisible Collision Hull" node just blocks the player.
	- It's often used behind level-end teleporters to prevent overshoot.
	- Avoid using it for windows instead glass-like nodes due to player confusion.
- The "Camera Focus" node sets the initial direction the player is looking when starting a level.
	- There should be only 1 in each level.
	- Place it reasonably near the player (less than a mapblock away)
- The "Level End Portal" node teleports the player to the next level (or the end credits on the last level).
- The "Reset Button" and "Reset Button Molly-Guard" are used to build the level reset buttons into the level terrain.
	- Follow the pattern and always have one molly-guard in front of each button.
	- Not mandatory if you are certain there is no way to become trapped or stuck in the level.
- The "Puzzle Chime" will play a special chime to all players in a level whenever a node face-touching it changes.
	- This is used to let the player know progress has been made.
	- Use sparingly, no more than a few times per level, focus on areas where the fact that progress has been made is not immediately obvious.
	- Groups of chime nodes all face-connected will fire off a single event once, if *any* member of the group is disturbed.
- All Slider nodes are obviously 
- Some nodes may have smaller special behaviors:
	- Fire and lava repel the player.
	- Fire and water emit ambient sounds.
	- Most nodes other than air will block the movement of klots.

## Install Map Changes into the Game

- Exit the editing world after all exports have quiesced.
- From the `modgen_mod_export` dir inside the world dir, copy the `manifest.json` file and `map` dir and all its contents over the corresponding items in `mods/klots_mapgen` within the game dir.
- To play test, create a new world *without* the klots_edit file.
	- For repeated testing, you can change the backend to `dummy` in that world's `world.mt`, causing play-time changes to the map to be reset each time you unload the area (leave the game) and return.  This makes rapid iteration a bit easier instead of having to reset the level manually each time.

## Submitting Levels for Official Inclusion

- Acceptable formats:
	- [worldedit](/packages/sfan5/worldedit/) format is good for the general case.
		- Please provide the exact coordinates at which it should be imported.
	- Submitting your `manifest.json` and `map/chunk_*.bin` files is also acceptable.
	- Pull/Merge Requests can also be accepted.
		- Please "rebase" your changes against latest master, because chunk*.bin files cannot be automatically merged.  This probably means saving your changes with worldedit, resetting to latest master, and then reimporting them back in place.
- Levels must fit somewhere in the progression, i.e. use existing skills learned in previous levels, and add some new challenge/insight.
- Levels must also fit the general theme and atmosphere of the game, and if possible should try to build on the narrative via environmental storytelling.
- Updates/improvements/fixes for existing levels are also welcome.
