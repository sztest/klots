-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

local screenshots = {}
for i = 1, 6 do screenshots[i] = readbinary('.cdb-screen' .. i .. '.webp') end

return {
	user = "Warr1024",
	pkg = "klots",
	version = stamp .. "-$Format:%h$",
	type = "game",
	dev_state = "BETA",
	title = "Klots",
	short_description = "Sliding block puzzles in an alien environment",
	tags = {"jam_game_2022", "mapgen", "oneofakind__original",
		"player_effects", "puzzle", "singleplayer"},
	license = "MIT",
	media_license = "CC-BY-SA-3.0",
	repo = "https://gitlab.com/sztest/klots",
	issue_tracker = "https://discord.gg/KMbCjAHBTs",
	long_description = readtext('README.md'),
	screenshots = screenshots
}

-- luacheck: pop
